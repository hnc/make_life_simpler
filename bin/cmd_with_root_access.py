#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr
# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys
import copy


## @brief Run command with root access
#  @param[in] cmd_arg_list Command
#  @return return new command
def cmd_with_root_access(cmd_arg_list):
	
	new_cmd_arg_list = copy.deepcopy(cmd_arg_list)
	
	# POSIX
	if (os.name == 'posix'):
		
		# OS X
		if (sys.platform == 'darwin'):
			
			new_cmd_arg_list = ['sudo'] + new_cmd_arg_list
			
		# GNU/Linux
		else:
			
			import grp
			import getpass
			
			if (getpass.getuser() in grp.getgrnam('sudo')):
				
				new_cmd_arg_list = ['sudo'] + new_cmd_arg_list
				
			else: 
				
				new_cmd_arg_list = ['su', '-c', ' '.join(new_cmd_arg_list)]
	
	return new_cmd_arg_list


from can_write_in import *


## @brief Run command with root access if we can not write in directory
#  @param[in] cmd_arg_list Command
#  @param[in] directory    Directory
#  @return return new command
def cmd_with_root_access_if_needed(cmd_arg_list, directory):
	
	# root access needed
	if (can_write_in(directory) == False):
		
		return cmd_with_root_access(cmd_arg_list);
	
	return copy.deepcopy(cmd_arg_list)


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print 'Usage:', sys.argv[0], "<command>"
		
		sys.exit(0)
	
	
	# Arguments
	command = sys.argv[1 : ]
	
	
	# Display
	print 'Command with root access =', cmd_with_root_access(command)
	
	
	sys.exit(0)
