#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


## @brief Return command to encode the audio file in mp3
#  @param[in] audio   Audio filename
#  @param[in] quality Quality (int between -1 and 10)
#  @return command to encode the audio file in mp3
def audio_encode_mp3(audio, quality):

	return 'lame "' + audio + '" -V ' + str(quality) + ' -o "' + audio + '.q' + str(quality) + '.mp3"'


import os
import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<audio.wav> [-q quality]'
		print ''
		print '' + '    ' + '-q quality = Quality between 9 and 0'
		print ''
		print 'Description:'
		print '    ' + 'Encode a audio in MP3 with lame'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	audio = sys.argv[1]
	quality_is_given = args.arg_exists('-q')
	quality = 0
	if (quality_is_given): quality = args.next_arg('-q')
	
	
	# Encode
	if (quality_is_given): 
		
		os.system(audio_encode_mp3(audio, quality))
		
	else:
		
		for i in range(0, 10):
			os.system(audio_encode_mp3(audio, i))
	
	sys.exit(0)
