#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


## @brief Return the command to shrink a pdf (with gs)
#  @param[in] input_filename         Input filename
#  @param[in] output_filename        Output filename
#  @param[in] color_image_resolution Color image resolution (150 by default)
#  @param[in] gray_image_resolution  Gray image resolution (same as color_image_resolution by default)
#  @param[in] mono_image_resolution  Mono image resolution (same as color_image_resolution by default)
#  @return the command to shrink a pdf
def pdf_shrink_cmd(input_filename, output_filename, color_image_resolution = 150, gray_image_resolution = 0, mono_image_resolution = 0):
	
	if (color_image_resolution == 0): color_image_resolution = 150
	if (gray_image_resolution == 0): gray_image_resolution = color_image_resolution
	if (mono_image_resolution == 0): mono_image_resolution = color_image_resolution
	
	cmd = "gs "
	cmd += "-q -dNOPAUSE -dBATCH -dSAFER "
	cmd += "-sDEVICE=pdfwrite "
	cmd += "-dCompatibilityLevel=1.3 "
	cmd += "-dPDFSETTINGS=/screen "
	cmd += "-dEmbedAllFonts=true "
	cmd += "-dSubsetFonts=true "
	cmd += "-dColorImageDownsampleType=/Bicubic "
	cmd += "-dColorImageResolution=" + color_image_resolution + " "
	cmd += "-dGrayImageDownsampleType=/Bicubic "
	cmd += "-dGrayImageResolution=" + gray_image_resolution + " "
	cmd += "-dMonoImageDownsampleType=/Bicubic "
	cmd += "-dMonoImageResolution=" + mono_image_resolution + " "
	cmd += "-sOutputFile=" + "\"" + output_filename + "\"" + " "
	cmd += "\"" + input_filename + "\""
	
	return cmd


import sys
import os.path

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<pdf file> [image_resolution (default = 150 dpi)]'
		print ''
		print 'Usage:', sys.argv[0], '<pdf file> [color_image_resolution gray_image_resolution mono_image_resolution]'
		print ''
		print 'Description:'
		print '    ' + 'Reduce the size of the pdf (by changing the images\' resolution)'
		print ''
		
		sys.exit(0)
	
	
	# Input & output
	input_filename = sys.argv[1]
	output_filename = os.path.splitext(os.path.basename(input_filename))[0] + "_shrink_dpi_"
	
	
	# DPI
	color_image_resolution = "150"
	gray_image_resolution = "150"
	mono_image_resolution = "150"
	
	# image_resolution
	if (len(sys.argv) == 3):
		
		color_image_resolution = sys.argv[2]
		gray_image_resolution = sys.argv[2]
		mono_image_resolution = sys.argv[2]
		
		output_filename += str(color_image_resolution) + ".pdf"
		
	# color_image_resolution gray_image_resolution mono_image_resolution
	elif (len(sys.argv) == 5):
		
		color_image_resolution = sys.argv[2]
		gray_image_resolution = sys.argv[3]
		mono_image_resolution = sys.argv[4]
		
		output_filename += str(color_image_resolution) + "_" + str(gray_image_resolution) + "_" + str(mono_image_resolution) + ".pdf"
		
	# End of output
	else:
		
		output_filename += str(color_image_resolution) + ".pdf"
	
	
	# Command
	cmd = pdf_shrink_cmd(input_filename, output_filename, color_image_resolution, gray_image_resolution, mono_image_resolution)
	
	
	# PDF shrink
	print ''
	print "Input   =", input_filename
	print "Output  =", output_filename
	print ''
	print "Command =", cmd
	print ''
	os.system(cmd)
	
	
	sys.exit(0)
