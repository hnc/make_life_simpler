#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


import sys
import os
import os.path

from make_life_simpler import args
from make_life_simpler.download import download


# Program
if __name__ == "__main__":
	
	# Help
	if args.help():
		
		print ''
		print 'Usage:', sys.argv[0]
		print ''
		print 'Description:'
		print '    ' + 'Install youtube-dl '
		print ''
		
		sys.exit(0)
	
	# Get path of script
	os.chdir(os.path.dirname(sys.argv[0]))
	
	# Download
	download('https://yt-dl.org/downloads/2014.07.11.3/', 'youtube-dl')
	
	# chmod
	os.system('chmod a+x youtube-dl')
	
	# Update
	os.system('./youtube-dl -U')
	
	sys.exit(0)
