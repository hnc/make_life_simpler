#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys

from make_life_simpler import args
from make_life_simpler.system import *
from make_life_simpler.ask_user import *
from make_life_simpler.abort_if_no import *

from cmd_with_root_access import *


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print 'Usage:', sys.argv[0], "<input_file.img>"
		
		sys.exit(0)
	
	
	# Arguments
	input_file = sys.argv[1]
	
	
	# fdisk -l
	system(cmd_with_root_access(['fdisk', '-l']))
	
	# Ask device
	device = str(raw_input('Device to be restored (e.g. /dev/sdX)? '))
	print ''
	abort_if_no(yes_no_question('Do you want restore device ' + device))
	print ''
	
	# Display size of input file
	print 'Size of', input_file, '=', os.path.getsize(input_file) / 1000, 'kB'
	
	# Restore device
	#system(cmd_with_root_access(['dd', 'if="' + input_file + '"', 'of="' + device + '"', 'conv=noerror']))
	system(cmd_with_root_access(['ddrescue', '--direct', '--synchronous', '--force', '"' + input_file + '"', '"' + device + '"', '"' + device[1:].replace('/', '_') + '.log"']))
	system('sync')
	
	sys.exit(0)
