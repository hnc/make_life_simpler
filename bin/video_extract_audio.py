#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


import subprocess

from video_get_maps import video_get_video_maps
from video_get_maps import video_get_audio_maps


## @brief Extract all audio tracks of a video in wav (with avconv)
#  @param[in] video Video filename
def video_extract_audio(video):
	
	audio_maps = video_get_audio_maps(video)
	
	for i in range(0, len(audio_maps)):
		
		print 'Extract audio map', audio_maps[i], '(' + str(i + 1) + ' / ' + str(len(audio_maps)) + ')'
		print ''
		
		subprocess.call(['avconv', '-i', video, '-map', audio_maps[i], '-acodec', 'pcm_s16le', video + '.' + audio_maps[i].replace(':', '-') + '.wav'])
		
		print ''


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<video>'
		print ''
		print 'Description:'
		print '    ' + 'Extract all audio tracks of a video in wav'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	video = sys.argv[1]
	
	
	# Extract
	print ''
	video_extract_audio(video)
	
	
	sys.exit(0)
