#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


## @brief Ask user to create filters for a video (with mplayer)
#  @param[in] video Video filename
#  @return the filters
def video_encode_create_filters(video):
	
	filters = ''
	
	filters += '[TODO] TODO'
	
	print  ''
	print  'Run mplayer "' + video + '" -vf cropdetect'
	print  ''
	print  'Other filters:'
	print  ''
	print  '- "yadif" filter to deinterlace'
	print  ''
	print  '- "1280:-1" to scale in 720p'
	print  ''
	print  '- "1920:-1" to scale in 1080p'
	print  ''
	
	return filters


import os
import os.path


## @brief Read filters for a video from files
#  @param[in] video           Video filename
#  @param[in] filters_keyword Keyword to identify if file describes filters ('filters' by default)
#  @return a list of filters (one by file)
def video_encode_read_filters_from_file(video, filters_keyword = 'filters'):
	
	filters = []
	
	directory = os.path.dirname(os.path.abspath(video))
	basename = os.path.splitext(os.path.basename(video))[0]
	
	for filename in os.listdir(directory):
		
		if (basename in filename):
			
			if (filters_keyword in filename):
				
				if (os.path.splitext(os.path.basename(filename))[1] == '.txt'):
				
					file_path = os.path.join(directory, filename)
					
					file = open(file_path, 'r')
					filters += [ file.readline() ]
					file.close()
	
	return filters


import sys
import os.path

from make_life_simpler import args

from filename_without_overwrite import filename_without_overwrite


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<video>'
		print ''
		print 'Description:'
		print '    ' + 'Ask user to create filters for a video'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	video = sys.argv[1]
	
	
	# Filters
	
	filters = video_encode_create_filters(video)
	
	print 'filters =', filters
	
	file = open(filename_without_overwrite(os.path.splitext(os.path.basename(video))[0] + '_filters.txt'), 'w')
	file.write(filters)
	file.close()
	
	sys.exit(0)
