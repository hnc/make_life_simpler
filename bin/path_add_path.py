#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


import sys

from make_life_simpler import args
from make_life_simpler import env


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<pathname> <--first || --last>'
		print ''
		print 'Description:'
		print '    ' + 'Add a pathname in the $PATH variable by editing the environment variables'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	pathname = sys.argv[1]
	first_or_last = args.arg_exists('--first')
	
	# Command
	env.env_variable_add_directory_in_config_file('PATH', pathname, first_or_last)
	
	
	sys.exit(0)
