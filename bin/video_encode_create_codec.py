#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


## @brief Create video codec for a video from tag
#  @param[in] tag Tag for the video codec
#  @return the video codec
def video_encode_create_video_codec(tag):
	
	# https://trac.ffmpeg.org/wiki/Encode/VP8
	# https://trac.ffmpeg.org/wiki/Encode/H.264
	# https://trac.ffmpeg.org/wiki/Encode/H.265
	# https://trac.ffmpeg.org/wiki/Encode/MPEG-4
	
	#if (tag == 'VP8_DVD'): return '[' + tag + '] ' + 'libvpx -deadline best -minrate 0 -maxrate 65536k -qmin 1 -qmax 63 -qcomp 1 -keyint_min 0 -g 360 -auto-alt-ref 1 -lag-in-frames 16 -slices 2 -mb_threshold 0'

	#if (tag == 'VP8_720p'): return '[' + tag + '] ' + 'libvpx -deadline best -minrate 0 -maxrate 65536k -qmin 1 -qmax 63 -qcomp 1 -keyint_min 0 -g 360 -auto-alt-ref 1 -lag-in-frames 16 -slices 4 -mb_threshold 0'

	#if (tag == 'VP8_1080p'): return '[' + tag + '] ' + 'libvpx -deadline best -minrate 0 -maxrate 65536k -qmin 1 -qmax 63 -qcomp 1 -keyint_min 0 -g 360 -auto-alt-ref 1 -lag-in-frames 16 -slices 8 -mb_threshold 0'
	
	#if (tag == 'h264'): return '[' + tag + '] ' + 'libx264 -preset placebo'
	
	if (tag == 'VP8'): return '[' + tag + '] ' + 'libvpx -minrate 0 -maxrate 65536k -qmin 1 -qmax 63 -crf 4'
	
	if (tag == 'VP9'): return '[' + tag + '] ' + 'libvpx-vp9 -minrate 0 -maxrate 65536k -qmin 1 -qmax 63 -crf 4'
	
	if (tag == 'h264'): return '[' + tag + '] ' + 'libx264 -preset placebo -crf 16'
	
	if (tag == 'h265'): return '[' + tag + '] ' + 'libx265 -preset placebo -crf 16'
	
	if (tag == 'xvid'): return '[' + tag + '] ' + 'libxvid -qscale:v 3'
	
	raise ValueError('\n\nvideo_create_video_codec(tag): tag "' + tag  + '" is not valid\n')


import os
import os.path


## @brief Read video codecs for a video from files
#  @param[in] video               Video filename
#  @param[in] video_codec_keyword Keyword to identify if file describes video codec ('video_codec' by default)
#  @return a list of video codecs (one by file)
def video_encode_read_video_codec_from_file(video, video_codec_keyword = 'video_codec'):
	
	video_codecs = []
	
	directory = os.path.dirname(os.path.abspath(video))
	basename = os.path.splitext(os.path.basename(video))[0]
	extension = os.path.splitext(os.path.basename(video))[1]
	
	for filename in os.listdir(directory):
		
		if (basename in filename):
			
			if (video_codec_keyword in filename):
				
				if (os.path.splitext(os.path.basename(filename))[1] == '.txt'):
				
					file_path = os.path.join(directory, filename)
					
					file = open(file_path, 'r')
					video_codecs += [ file.readline() ]
					file.close()
	
	return video_codecs


import sys
import os.path

from make_life_simpler import args

from filename_without_overwrite import filename_without_overwrite


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help() or args.nb_arg() != 2:
		
		print ''
		print 'Usage:', sys.argv[0], '<video> <tag>'
		print ''
		print 'Description:'
		print '    ' + 'Create video codec for a video from tag'
		print ''
		print 'Tags:'
		print ''
		print '    ' + 'VP8'
		print '    ' + 'VP9'
		print '    ' + 'h264'
		print '    ' + 'h265'
		print '    ' + 'xvid'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	video = sys.argv[1]
	tag = sys.argv[2]
	
	
	# Filters
	
	video_codec = video_encode_create_video_codec(tag)
	
	print 'video_codec =', video_codec
	
	file = open(filename_without_overwrite(os.path.splitext(os.path.basename(video))[0] + '_video_codec_' + tag + '.txt'), 'w')
	file.write(video_codec)
	file.close()
	
	sys.exit(0)
