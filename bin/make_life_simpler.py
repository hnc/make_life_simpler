#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.help():
		
		print 'Usage:', sys.argv[0]
		print ''
		print 'Description:'
		print ''
		print '    ' + 'Make life simpler (with Debian GNU/Linux)'
		print ''
		print '    ' + 'List of the licenses actually used:'
		print '    ' + '- Apache License, Version 2.0'
		print '    ' + '- GNU Affero General Public License 3+'
		print ''
		print '    ' + 'Make life simpler is a collection of scripts'
		print '    ' + 'to simplify the use of your computer'
		print ''
		
		sys.exit(0)
	
	
	# You can test make_life_simpler in this code
	
	
	sys.exit(0)
