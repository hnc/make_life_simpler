#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


import os.path


## @brief Return the tag for video_cut
#  @param[in] video Video filename
#  @param[in] start Time when video starts (time_in_ffmpeg_format example: 01:42:21.500)
#  @param[in] time  Duration time of the video
#  @return the tag for video_cut
def video_cut_tag(video, start, time):
	
	tag = ''
	
	if (start == ''): tag += 'begin'
	else: tag += start
	tag += '_'
	
	if (time == ''): tag += 'end'
	else: tag += time
	
	return tag.replace(':', '.')


## @brief Return the output filename for video_cut
#  @param[in] video Video filename
#  @param[in] start Time when video starts (time_in_ffmpeg_format example: 01:42:21.500)
#  @param[in] time  Duration time of the video
#  @return the output filename for video_cut
def video_cut_output_filename(video, start, time):
	
	output_filename = os.path.splitext(os.path.basename(video))[0] + '_'
	
	output_filename += video_cut_tag(video, start, time)
	
	return output_filename


## @brief Return subvideo tag (--start, -t)
#  @param[in] video Video filename
#  @return the subvideo tag
def video_encode_create_subvideo(video, start, time):
	
	subvideo = ''
	
	if (start != ''): subvideo += '-ss ' + start
	
	if (start != '' and time != ''): subvideo += ' '
	
	if (time != ''): subvideo += '-t ' + time
	
	return '[' + video_cut_tag(video, start, time) + '] ' + subvideo


import os
import os.path


## @brief Read subvideo for a video from files
#  @param[in] video            Video filename
#  @param[in] subvideo_keyword Keyword to identify if file describes subvideo ('subvideo' by default)
#  @return a list of subvideo (one by file)
def video_encode_read_subvideo_from_file(video, subvideo_keyword = 'subvideo'):
	
	subvideos = []
	
	directory = os.path.dirname(os.path.abspath(video))
	basename = os.path.splitext(os.path.basename(video))[0]
	
	for filename in os.listdir(directory):
		
		if (basename in filename):
			
			if (subvideo_keyword in filename):
				
				if (os.path.splitext(os.path.basename(filename))[1] == '.txt'):
				
					file_path = os.path.join(directory, filename)
					
					file = open(file_path, 'r')
					subvideos += [ file.readline() ]
					file.close()
	
	return subvideos


from video_get_maps import video_get_video_maps
from video_get_maps import video_get_audio_maps


## @brief Return the command to cut video (with avconv)
#  @param[in] video                Video filename
#  @param[in] start                Time when video starts (time_in_ffmpeg_format example: 01:42:21.500)
#  @param[in] time                 Duration time of the video
#  @param[in] one_video_track_only Keep first video track only
#  @param[in] one_audio_track_only Keep first audio track only
#  @return the command to cut video
def video_cut_cmd(video, start, time, one_video_track_only, one_audio_track_only):
	
	output_filename = video_cut_output_filename(video, start, time)
	
	cmd = 'avconv -i "' + video + '" '
	
	if (start != ''): cmd += '-ss ' + start + ' '
	
	if (time != ''): cmd += '-t ' + time + ' '
	
	if (one_video_track_only == False):
		
		for video_map in video_get_video_maps(video):
			cmd += '-map' + ' ' + video_map + ' '
	
	if (one_audio_track_only == False):
		
		for audio_map in video_get_audio_maps(video):
			cmd += '-map' + ' ' + audio_map + ' '
	
	cmd += '-acodec pcm_s16le '
	
	cmd += '-vcodec copy '
	
	cmd += '"' + output_filename + '.avi"'
	
	return cmd


import sys
import os

from make_life_simpler import args

from filename_without_overwrite import filename_without_overwrite


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<video> [--start <time_in_ffmpeg_format>] [-t <time_in_ffmpeg_format>] [--video] [--audio]'
		print ''
		print '    ' + '--start = Time when video starts (time_in_ffmpeg_format example: 01:42:21.500)'
		print ''
		print '    ' + '-t      = Duration time of the video'
		print ''
		print '    ' + '--video = Keep first video track only'
		print ''
		print '    ' + '--audio = Keep first audio track only'
		print ''
		print 'Description:'
		print '    ' + 'Cut a video between start and start + t times.'
		print '    ' + 'For convenience, all audio tracks are converted in wav.'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	video = sys.argv[1]
	start = args.next_arg("--start")
	time = args.next_arg("-t")
	
	
	# Cut video
	cmd = video_cut_cmd(video, start, time, args.arg_exists('--video'), args.arg_exists('--audio'))
	print cmd
	os.system(cmd)
	
	# Save in file
	subvideo = video_encode_create_subvideo(video, start, time)
	file = open(filename_without_overwrite(os.path.splitext(os.path.basename(video))[0] + '_[subvideo]_' + video_cut_tag(video, start, time) + '.txt'), 'w')
	file.write(subvideo)
	file.close()
	
	
	sys.exit(0)
