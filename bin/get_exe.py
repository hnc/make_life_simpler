#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr
# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import copy
import os

from make_life_simpler import ask_user

from exe_is_available import *


## @brief Return path of executable (abort if not found)
#  @param[in] exe Executable path
#  @return the path of the executable if it is available, abort otherwise
def get_exe(exe, windows_exe = ''):
	
	cmd = copy.deepcopy(exe)
	
	if (os.name == 'nt'):
		
		if (windows_exe == ''): cmd += '.exe'
		else: cmd = copy.deepcopy(windows_exe)
	
	ask_user.abort_if_no(exe_is_available_with_print(cmd))
	
	return cmd

## @brief Return path of unzip executable (abort if not found)
#  @return the path of the unzip executable if it is available, abort otherwise
def unzip(): return get_exe('unzip')

## @brief Return path of tar executable (abort if not found)
#  @return the path of the tar executable if it is available, abort otherwise
def tar(): return get_exe('tar')

## @brief Return path of cmake executable (abort if not found)
#  @return the path of the cmake executable if it is available, abort otherwise
def cmake(): return get_exe('cmake')

## @brief Return path of make executable (abort if not found)
#  @return the path of the make executable if it is available, abort otherwise
def make(): return get_exe('make', 'mingw32-make.exe')


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print 'Usage:', sys.argv[0], "<executable name>"
		
		sys.exit(0)
	
	
	# Arguments
	exe = sys.argv[1]
	
	
	# Display
	get_exe(exe)
	
	
	sys.exit(0)
