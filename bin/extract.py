#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr
# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import os.path

from get_exe import *


## @brief Extract the archive
#  @param[in] archive_filename Archive
def extract(archive_filename):
	
	extension = os.path.splitext(archive_filename)[1]
	
	if (extension == '.zip'):
		
		os.system(unzip() + ' ' + archive_filename)
		
	elif ('.tar' in archive_filename and (extension == '.gz' or extension == '.bz' or extension == '.bz2' or extension == '.lz' or extension == '.xz' or extension == '.7z' or extension == '.lzma')):
		
		os.system(tar() + ' -xf ' + archive_filename)
		
	else:
		print 'Can not extract file "' + archive_filename + '" (extension "' + extension + '" is not supported)'


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print 'Usage:', sys.argv[0], "<archive>"
		
		sys.exit(0)
	
	
	# Arguments
	archive = sys.argv[1]
	
	
	# Display
	extract(archive)
	
	
	sys.exit(0)
