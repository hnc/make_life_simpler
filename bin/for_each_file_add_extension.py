#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys

from make_life_simpler import args

from add_extension import *
from for_each_file import *


# Program
if __name__ == "__main__":
	
	# Help
	if len(sys.argv) <= 2 or args.help():
		
		print 'Usage:', sys.argv[0], "<directory> <extension> [-r]"
		
		sys.exit(0)
	
	
	# Arguments
	directory = sys.argv[1]
	extension = sys.argv[2]
	recursive = args.arg_exists('-r')
	
	
	# Add extension for each file
	def add_extension_wanted(file): add_extension(file, extension)
	for_each_file(directory, add_extension_wanted, recursive)
	
	
	sys.exit(0)
