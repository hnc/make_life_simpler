#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


import os.path


from video_get_maps import video_get_video_maps
from video_get_maps import video_get_audio_maps


## @brief Return the command to encode a video
#  @param[in] video         Video filename
#  @param[in] subvideo      Subvideo
#  @param[in] video_codec   Video codec
#  @param[in] video_bitrate Bitrate
#  @param[in] filters       Filters
def video_encode_cmd(video, subvideo, video_codec, video_bitrate, filters):
	
	subvideo_tag = subvideo[ : subvideo.index(' ')]
	subvideo_value = subvideo[subvideo.index(' ') + 1 : ]
	
	video_codec_tag = video_codec[ : video_codec.index(' ')]
	video_codec_value = video_codec[video_codec.index(' ') + 1 : ]
	
	video_bitrate_tag = video_bitrate[ : video_bitrate.index(' ')]
	video_bitrate_value = video_bitrate[video_bitrate.index(' ') + 1 : ]
	
	filters_tag = filters[ : filters.index(' ')]
	filters_value = filters[filters.index(' ') + 1 : ]
	
	output_filename = os.path.splitext(os.path.basename(video))[0] + '_' + subvideo_tag + '_' + video_codec_tag + '_' + video_bitrate_tag
	if (filters_tag != ''): output_filename += '_' + filters_tag
	
	cmd = 'avconv -i "' + video + '"' + ' '
	
	if (subvideo_value != ''):
		cmd += subvideo_value + ' '
	
	if (filters_value != ''):
		cmd += filters_value + ' '
	
	for video_map in video_get_video_maps(video):
		cmd += '-map' + ' ' + video_map + ' '
	
	for audio_map in video_get_audio_maps(video):
		cmd += '-map' + ' ' + audio_map + ' '
	
	cmd += '-acodec pcm_s16le '
	
	cmd += '-vcodec ' + video_codec_value + ' '
	
	cmd += '-threads 4 '
	
	cmd += '-vb ' + video_bitrate_value + ' '
	
	#return [ cmd + '-pass 1 "' + output_filename + '_pass_1.avi"', cmd + '-pass 2 "' + output_filename + '_pass_2.avi"' ]
	return [ cmd + '-pass 1 "' + output_filename + '_pass_1.avi"', cmd + '-pass 2 "' + output_filename + '_pass_2.avi"' ]


import sys

from make_life_simpler import args

from video_cut import video_encode_read_subvideo_from_file
from video_encode_create_codec import video_encode_read_video_codec_from_file
from video_encode_create_bitrate import video_encode_read_video_bitrate_from_file
from video_encode_create_filters import video_encode_read_filters_from_file


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<video>'
		print ''
		print 'Description:'
		print '    ' + 'Encode a video'
		print '    ' + 'For convenience, all audio tracks are converted in wav.'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	video = sys.argv[1]
	
	
	# Encode
	
	cmds = []
	
	subvideos = video_encode_read_subvideo_from_file(video)
	
	if (subvideos == []):
		subvideos += [ '[begin_end] ' ]
	
	video_codecs = video_encode_read_video_codec_from_file(video)
	
	if (video_codecs == []):
		raise Exception('\n\nNo video codec found, please run:\n    video_encode_create_codec.py "' + video + '"\n')
	
	video_bitrates = video_encode_read_video_bitrate_from_file(video)
	
	if (video_bitrates == []):
		raise Exception('\n\nNo video bitrate found, please run:\n    video_encode_create_bitrate.py "' + video + '"\n')
	
	filterss = video_encode_read_filters_from_file(video)
	
	if (filterss == []):
		print '\n\nNo video filters found, you can run:\n    video_encode_create_filters.py "' + video + '"'
		filterss += [ '[no_filters] ' ]
	
	for subvideo in subvideos:
	
		for video_codec in video_codecs:
			
			for video_bitrate in video_bitrates:
			
				for filters in filterss:
					
					cmds += [ video_encode_cmd(video, subvideo, video_codec, video_bitrate, filters) ]
	
	output_filename = video + '_encode.py'
	
	file = open(output_filename, 'w')
	
	file.write('#!/usr/bin/python\n')
	file.write('# -*- coding: utf-8 -*-\n')
	file.write('\n')
	file.write('import os\n')
	file.write('\n')
	
	for cmd in cmds:
		
		file.write('os.system(\'' + cmd[0] + '\')\n')
		file.write('os.system(\'' + cmd[1] + '\')\n')
		file.write('\n')
	
	file.close()
	
	print ''
	print 'Save script for the enconding in: "' + output_filename + '"'
	print ''
	print 'You can run: python "' + output_filename + '"'
	print ''
	
	
	sys.exit(0)
