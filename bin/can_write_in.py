#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr
# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import os.path


## @brief Can write in directory?
#  @param[in] directory Directory
#  @return True if we can write in directory, False otherwise
def can_write_in(directory):
	
	test_filename = os.path.join(directory, 'make_life_simpler_test_if_write_is_possible')
	
	# Can write in file in directory
	try:
		
		tmp = open(test_filename, 'w+')
		tmp.close()
		os.remove(test_filename)
		
		return True
		
	# Can not write in file in directory
	except:
		
		return False


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print 'Usage:', sys.argv[0], "<directory>"
		
		sys.exit(0)
	
	
	# Arguments
	directory = sys.argv[1]
	
	
	# Display
	print 'Can write in "' + directory + '" =', can_write_in(directory)
	
	
	sys.exit(0)
