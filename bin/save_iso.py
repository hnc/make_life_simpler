#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys

from make_life_simpler import args
from make_life_simpler.system import *
from make_life_simpler.ask_user import *
from make_life_simpler.abort_if_no import *

from cmd_with_root_access import *


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print 'Usage:', sys.argv[0], "<output_file.iso>"
		
		sys.exit(0)
	
	
	# Arguments
	output_file = sys.argv[1]
	
	
	# List cdrom
	print 'List of cdrom ='
	for file in os.listdir('/dev'):
		pathfile = os.path.join('/dev', file)
		if ('/dev/cdrom' in pathfile):
			print pathfile
	print
	
	# Ask device
	device = str(raw_input('Device to be saved (e.g. /dev/cdromX)? '))
	print ''
	abort_if_no(yes_no_question('Do you want save device ' + device))
	print ''
	
	# Save device
	system(cmd_with_root_access(['ddrescue', '--direct', '--synchronous', '--reopen-on-error', '--retry-passes=512', '"' + device + '"', '"' + output_file + '"', '"' + output_file + '.log"']))
	system('sync')
	
	sys.exit(0)
