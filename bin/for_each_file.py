#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr
# Copyright © 2014-2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import os.path

from make_life_simpler.system import *


## @brief Excecute a command for each file
#  @param[in] path          A directory name
#  @param[in] function      Function to be executed for each file
#  @param[in] recursive     True if recursive, False otherwise (False by default)
#  @param[in] ignore_hidden True if ignore hidden, False otherwise (False by default)
def for_each_file(path, function, recursive = False, ignore_hidden = False):
	
	for file in os.listdir(path):
		
		pathfile = os.path.join(path, file)
		
		if (os.path.basename(pathfile)[0] != '.'):
			
			if (os.path.isdir(pathfile)):
				
				if (recursive): for_each_file(pathfile, function, recursive, ignore_hidden)
				
			else:
				
				function(pathfile)


## @brief Excecute a command for each file
#  @param[in] path      A directory name
#  @param[in] command   Command to be executed for each file
#  @param[in] recursive True if recursive, False otherwise (False by default)
def for_each_file_command(path, command, recursive = False):
	
	for file in os.listdir(path):
		
		pathfile = os.path.join(path, file)
		
		if (os.path.isdir(pathfile)):
			
			if (recursive): for_each_file_command(pathfile, command, recursive)
			
		else:
			
			system_halt_on_error([command, pathfile])


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if len(sys.argv) <= 2 or args.help():
		
		print 'Usage:', sys.argv[0], "<directory> <command> [-r]"
		
		sys.exit(0)
	
	
	# Arguments
	directory = sys.argv[1]
	command = sys.argv[2]
	recursive = args.arg_exists('-r')
	
	
	# Excecute
	for_each_file(directory, command, recursive)
	
	
	sys.exit(0)
