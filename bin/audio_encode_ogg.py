#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


## @brief Return command to encode the audio file in ogg
#  @param[in] audio   Audio filename
#  @param[in] quality Quality (int between -1 and 10)
#  @return command to encode the audio file in ogg
def audio_encode_ogg(audio, quality):

	return 'oggenc "' + audio + '" -q ' + str(quality) + ' -o "' + audio + '.q' + str(quality) + '.ogg"'


import os
import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<audio.wav> [-q quality]'
		print ''
		print '' + '    ' + '-q quality = Quality between -1 and 10'
		print ''
		print 'Description:'
		print ''
		print '    ' + 'Encode a audio in ogg vorbis with oggenc'
		print ''
		print '    ' + 'ogg_quality = q-1  45 kbit/s | q0  64 kbit/s | q1  80 kbit/s |  q2  96 kbit/s'
		print '    ' + '               q3 112 kbit/s | q4 128 kbit/s | q5 160 kbit/s |  q6 192 kbit/s'
		print '    ' + '               q7 224 kbit/s | q8 256 kbit/s | q9 320 kbit/s | q10 500 kbit/s'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	audio = sys.argv[1]
	quality_is_given = args.arg_exists('-q')
	quality = 0
	if (quality_is_given): quality = args.next_arg('-q')
	
	
	# Encode
	if (quality_is_given): 
		
		os.system(audio_encode_ogg(audio, quality))
		
	else:
		
		for i in range(-1, 11):
			os.system(audio_encode_ogg(audio, i))
	
	sys.exit(0)
