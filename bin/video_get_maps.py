#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


import copy


## @brief Return the map number of a word
#  @param[in] word Word contains map number
#  @return the map number
def extract_map_number(word):
	
	map_number = copy.deepcopy(word)
	
	map_number = map_number[map_number.index('#') + 1 : len(map_number) - 1]
	
	if (map_number.find('[') != -1):
		map_number = map_number[ : map_number.index('[')]
	
	if (map_number.find('(') != -1):
		map_number = map_number[ : map_number.index('(')]
	
	return map_number


import subprocess


## @brief Return the audio maps of a video (with avconv)
#  @param[in] video Video filename
#  @return the audio maps of a video
def video_get_audio_maps(video):
	
	p = subprocess.Popen(['avconv', '-i', video], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = p.communicate()
	
	out += '\n' + err
	
	audio_maps = []
	
	for line in out.splitlines():
		
		words = line.split(' ')
		
		for i in range(len(words)):
			
			if (words[i] == 'Stream'):
				
				if (words[i + 2] == 'Audio:'):
				
					audio_maps += [extract_map_number(words[i + 1])]
	
	return audio_maps


## @brief Return the video maps of a video (with avconv)
#  @param[in] video Video filename
#  @return the video maps of a video
def video_get_video_maps(video):
	
	p = subprocess.Popen(['avconv', '-i', video], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = p.communicate()
	
	out += '\n' + err
	
	video_maps = []
	
	for line in out.splitlines():
		
		words = line.split(' ')
		
		for i in range(len(words)):
			
			if (words[i] == 'Stream'):
				
				if (words[i + 2] == 'Video:'):
				
					video_maps += [extract_map_number(words[i + 1])]
	
	return video_maps


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<video>'
		print ''
		print 'Description:'
		print '    ' + 'Print the audio maps of a video'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	video = sys.argv[1]
	
	
	# Display
	print 'video_maps =', video_get_video_maps(video)
	print 'audio_maps =', video_get_audio_maps(video)
	
	
	sys.exit(0)
