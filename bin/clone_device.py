#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys

from make_life_simpler import args
from make_life_simpler.system import *
from make_life_simpler.ask_user import *
from make_life_simpler.abort_if_no import *

from cmd_with_root_access import *


# Program
if __name__ == "__main__":
	
	# fdisk -l
	system(cmd_with_root_access(['fdisk', '-l']))
	
	# Ask device
	source = str(raw_input('Device to be cloned (e.g. /dev/sdX)? '))
	destination = str(raw_input('Where do you want clone device ' + source + ' (e.g. /dev/sdX)? '))
	print ''
	abort_if_no(source != destination, 'Can not clone ' + source + ' into ' + destination + '!')
	abort_if_no(yes_no_question('Do you want clone device ' + source + ' into ' + destination))
	print ''
	
	# Output file
	output_file = '"' + 'clone_' + source + '_into_' + destination + '.log' + '"'
	output_file = output_file.replace('/', '_')
	output_file = output_file.replace('__', '_')
	
	# Clone
	system(cmd_with_root_access(['ddrescue', '--force', source, destination, output_file]))
	system('sync')
	
	sys.exit(0)
