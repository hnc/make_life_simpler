#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


## @brief Create video bitrate for a video from tag
#  @param[in] tag Tag for the video bitrate
#  @return the video bitrate
def video_encode_create_video_bitrate(tag):
	
	# 1024k 1536k 2048k 3072k 4096k 5120k 6144k 7168k 8192k 16384k 32768k 65536k
	
	if (tag == 'good_360p'): return '[' + tag + '] ' + '1536k'
	if (tag == 'good_DVD'): return '[' + tag + '] ' + '2048k'
	if (tag == 'good_720p'): return '[' + tag + '] ' + '4096k'
	if (tag == 'good_1080p'): return '[' + tag + '] ' + '6144k'
	
	if (tag == 'ok_360p'): return '[' + tag + '] ' + '1024k'
	if (tag == 'ok_DVD'): return '[' + tag + '] ' + '1536k'
	if (tag == 'ok_720p'): return '[' + tag + '] ' + '2048k'
	if (tag == 'ok_1080p'): return '[' + tag + '] ' + '4096k'
	
	if (tag == '1024k'): return '[' + tag + '] ' + '1024k'
	if (tag == '1536k'): return '[' + tag + '] ' + '1536k'
	if (tag == '2048k'): return '[' + tag + '] ' + '2048k'
	if (tag == '4096k'): return '[' + tag + '] ' + '4096k'
	if (tag == '6144k'): return '[' + tag + '] ' + '6144k'
	if (tag == '7168k'): return '[' + tag + '] ' + '7168k'
	if (tag == '8192k'): return '[' + tag + '] ' + '8192k'
	if (tag == '9216k'): return '[' + tag + '] ' + '9216k'
	if (tag == '10240k'): return '[' + tag + '] ' + '10240k'
	if (tag == '12288k'): return '[' + tag + '] ' + '12288k'
	if (tag == '16384k'): return '[' + tag + '] ' + '16384k'
	
	raise ValueError('\n\nvideo_create_video_bitrate(tag): tag "' + tag  + '" is not valid\n')


import os
import os.path


## @brief Read video bitrates for a video from files
#  @param[in] video               Video filename
#  @param[in] video_bitrate_keyword Keyword to identify if file describes video bitrate ('video_bitrate' by default)
#  @return a list of video bitrates (one by file)
def video_encode_read_video_bitrate_from_file(video, video_bitrate_keyword = 'video_bitrate'):
	
	video_bitrates = []
	
	directory = os.path.dirname(os.path.abspath(video))
	basename = os.path.splitext(os.path.basename(video))[0]
	extension = os.path.splitext(os.path.basename(video))[1]
	
	for filename in os.listdir(directory):
		
		if (basename in filename):
			
			if (video_bitrate_keyword in filename):
				
				if (os.path.splitext(os.path.basename(filename))[1] == '.txt'):
				
					file_path = os.path.join(directory, filename)
					
					file = open(file_path, 'r')
					video_bitrates += [ file.readline() ]
					file.close()
	
	return video_bitrates


import sys
import os.path

from make_life_simpler import args

from filename_without_overwrite import filename_without_overwrite


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help() or args.nb_arg() != 2:
		
		print ''
		print 'Usage:', sys.argv[0], '<video> <tag>'
		print ''
		print 'Description:'
		print '    ' + 'Create video bitrate for a video from tag'
		print ''
		print 'Tags (bitrate depends on quality wanted and video resolution):'
		print ''
		print '    ' + '.-' + '----------' + '-+-' + '------------' + '---' + '----------' + '-.'
		print '    ' + '| ' + 'Resolution' + ' | ' + 'Good quality' + ' | ' + 'Ok quality' + ' |'
		print '    ' + '|-' + '----------' + '-+-' + '------------' + '-+-' + '----------' + '-|'
		print '    ' + '| ' + '     360p ' + ' | ' + '      1536k ' + ' | ' + '    1024k ' + ' |'
		print '    ' + '|-' + '----------' + '-+-' + '------------' + '-+-' + '----------' + '-|'
		print '    ' + '| ' + '      DVD ' + ' | ' + '      2048k ' + ' | ' + '    1536k ' + ' |'
		print '    ' + '|-' + '----------' + '-+-' + '------------' + '-+-' + '----------' + '-|'
		print '    ' + '| ' + '     720p ' + ' | ' + '      4096k ' + ' | ' + '    2048k ' + ' |'
		print '    ' + '|-' + '----------' + '-+-' + '------------' + '-+-' + '----------' + '-|'
		print '    ' + '| ' + '    1080p ' + ' | ' + '      6144k ' + ' | ' + '    4096k ' + ' |'
		print '    ' + '--' + '----------' + '---' + '------------' + '---' + '----------' + '--'
		print ''
		print 'Other tags = 7168k 8192k 9216k 10240k 12288k 16384k'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	video = sys.argv[1]
	tag = sys.argv[2]
	
	
	# Filters
	
	video_bitrate = video_encode_create_video_bitrate(tag)
	
	print 'video_bitrate =', video_bitrate
	
	file = open(filename_without_overwrite(os.path.splitext(os.path.basename(video))[0] + '_video_bitrate_' + tag + '.txt'), 'w')
	file.write(video_bitrate)
	file.close()
	
	sys.exit(0)
