#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os.path


## @brief Return a available filename
#  @param[in] pathname_wanted Pathname wanted
#  @param[in] separator       Separator between pathname wanted and number is the function generate a filename ('_' by default)
#  @return a available filename
def filename_without_overwrite(pathname_wanted, separator = '_'):
	
	if os.path.isfile(pathname_wanted) == False:
		
		return pathname_wanted
	
	else:
		
		i = 0
		
		available_pathname = ''
		
		extension = os.path.splitext(os.path.basename(pathname_wanted))[1]
		basename = os.path.splitext(os.path.basename(pathname_wanted))[0]
		
		while True:
			
			available_pathname = os.path.join(os.path.dirname(pathname_wanted), basename)
			available_pathname += separator
			available_pathname += str(i)
			if (extension != ''): available_pathname += extension
			
			if (os.path.isfile(available_pathname) == False): break
			
			i += 1
			
		return available_pathname


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<filename>'
		print ''
		print 'Description:'
		print '    ' + 'Return a available filename.'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	filename = sys.argv[1]
	
	
	# Display
	print filename_without_overwrite(filename)
	
	
	sys.exit(0)
