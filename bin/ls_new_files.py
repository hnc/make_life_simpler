#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys
import datetime

from make_life_simpler import args
from make_life_simpler.hash import *

from for_each_file import *


## @brief List new files
class ls_new_files:
	
	## @brief Constructor
	#  @param[in] self              A ls_new_files
	#  @param[in] working_directory Working directory
	#  @param[in] force             True to force md5 computation, False otherwise
	def __init__(self, working_directory, force):
		
		self.force = force
		
		# Path
		self.working_directory = working_directory
		self.log_directory = os.path.join(self.working_directory, 'log')
		self.log_filename = os.path.join(self.log_directory, 'changelog.txt')
		
		# New files
		self.files = []
		
		# Old files
		self.old_files = []
		dirname = '.'
		if os.path.exists(self.log_filename):
			log_file = open(self.log_filename, 'r')
			for line in log_file:
				if (line == '\n'): continue
				elif (line[0] == '#'): continue
				elif (line[0:11] == 'directory ='): dirname = line[12:len(line) - 1]
				else:
					md5 = line[0:32]
					basename = line[33:len(line) - 1]
					old_file = self.old_files_index(dirname, basename)
					if (old_file == None):
						self.old_files += [[dirname, basename, md5]]
					else:
						old_file[2] = md5
			log_file.close()
		else:
			if (os.path.exists(self.log_directory) == False): os.mkdir(self.log_directory)
			open(self.log_filename, 'w').close()
		self.old_files = sorted(self.old_files)
		
		# Datetime
		self.now = datetime.datetime.now()
	
	## @brief Return index of a file in self.old_files list
	#  @param[in] self A ls_new_files
	#  @param[in] dirname  Dirname
	#  @param[in] basename Basename
	#  @return index of a file in self.old_files list, None if not found
	def old_files_index(self, dirname, basename):
		
		for dirname_basename_md5 in self.old_files:
			if (dirname_basename_md5[0] == dirname and dirname_basename_md5[1] == basename):
				return dirname_basename_md5
		
		return None
	
	## @brief List new files
	#  @param[in] self A ls_new_files
	#  @param[in] filename Path of filename
	def __call__(self, filename):
		
		if (os.path.dirname(filename) != self.log_directory):
			
			dirname = os.path.dirname(filename)
			basename = os.path.basename(filename)
			
			old_file = self.old_files_index(dirname, basename)
			
			md5_new = ''
			if old_file == None or self.force:
				print 'Compute md5 of ' + filename + '...',
				sys.stdout.flush()
				md5_new = md5_of_file(filename)
				print md5_new
			else:
				md5_new = old_file[2]
			
			if (old_file == None or old_file[2] != md5_new):
				
				print '- added (or updated)'
				
				self.files += [[dirname, basename, md5_new]]
				self.files = sorted(self.files)
	
	## @brief Files have been added or updated?
	#  @param[in] self A ls_new_files
	#  @return True if files have been added or updated, False otherwise
	def files_added_or_updated(self):
		
		if (len(self.files) != 0): return True
		else: return False
	
	## @brief Return title with datetime
	#  @param[in] self A ls_new_files
	#  @return title with datetime
	def title(self):
		
		r = ''
		r += '#  --------------------\n'
		r += '# | '
		for c in self.now.strftime("%Y/%m/%d - %Hh%M"): r += c
		r += ' |\n'
		r += '#  --------------------\n'
		
		return r
	
	## @brief Return the list of new files
	#  @param[in] self A ls_new_files
	#  @return the list of new files
	def new_files(self):
		
		r = ''
		
		genre = ''
		
		for dirname_basename_md5 in self.files:
			
			if (genre != dirname_basename_md5[0] and genre != ''): r += '\n'
			
			if (genre != dirname_basename_md5[0]):
				genre = dirname_basename_md5[0]
				genre_comment = genre
				if (len(genre_comment) >= 2 and genre_comment[0:2] == './'):
					genre_comment = genre_comment[2:]
				genre_comment = genre_comment.replace('/', ', ')
				r += '# ' + genre_comment + '\n'
				r += '# '
				for c in genre_comment: r += '-'
				r += '\n'
				r += '\n'
				r += 'directory = ' + genre + '\n'
			
			r += dirname_basename_md5[2] + ' ' + dirname_basename_md5[1] + '\n'
		
		if (len(self.files) == 0): r += '# Already up-to-date\n'
		
		return r
	
	## @brief Return the list of new files
	#  @param[in] self A ls_new_files
	#  @return the list of new files
	def changelog(self):
		
		r = ''
		r += self.title() + '\n'
		r += self.new_files()
		return r
	
	## @brief Write changelog in log/changelog.txt
	#  @param[in] self A ls_new_files
	def write_changelog(self):
		
		log_file = open(self.log_filename, 'a')
		
		log_file.write(self.changelog() + '\n')
		
		log_file.close()


# Program
if __name__ == "__main__":
	
	# Help
	if len(sys.argv) <= 1 or args.help():
		
		print 'Usage:', sys.argv[0], "<directory> [-r] [-f]"
		
		sys.exit(0)
	
	
	# Arguments
	directory = os.path.expanduser(sys.argv[1])
	recursive = args.arg_exists('-r')
	force = args.arg_exists('-f')
	
	# List new files
	ls = ls_new_files(directory, force)
	for_each_file(directory, ls, recursive)
	print ''
	
	# Print & Write changelog
	print ls.changelog()
	if (ls.files_added_or_updated()): ls.write_changelog()
	
	sys.exit(0)
