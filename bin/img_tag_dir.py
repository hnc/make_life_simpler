#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import os.path

from make_life_simpler import args
from make_life_simpler.img_tag import *


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<image tag>'
		print ''
		print 'Description:'
		print '    ' + 'Compose tag image over images'
		print ''
		
		sys.exit(0)
	
	
	# Input & output
	img_tag = sys.argv[1]
	
	
	# Tags
	img_tag_dir('.', img_tag)
	
	
	sys.exit(0)
