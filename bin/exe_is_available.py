#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr
# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import distutils.spawn


## @brief Return path of executable
#  @param[in] exe Executable path
#  @return the path of the executable if it is available, None otherwise
def exe_path(exe):
	
	return distutils.spawn.find_executable(exe)


## @brief Executable is available?
#  @param[in] exe Executable path
#  @return True if executable is available, False otherwise
def exe_is_available(exe):
	
	return exe_path(exe) != None


## @brief Executable is available? (with print)
#  @param[in] exe Executable path
#  @return True if executable is available, False otherwise
def exe_is_available_with_print(exe):
	
	r = exe_is_available(exe)
	
	if exe_is_available(exe):
		print 'Executable "' + exe + '"', 'found at', '"' + exe_path(exe) + '"'
	else:
		print 'Executable "' + exe + '"', 'not found :('
	
	return r


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print 'Usage:', sys.argv[0], "<executable name>"
		
		sys.exit(0)
	
	
	# Arguments
	exe = sys.argv[1]
	
	
	# Display
	exe_is_available_with_print(exe)
	
	
	sys.exit(0)
