#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import os.path

from make_life_simpler import args
from make_life_simpler import env


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help() or args.nb_arg() < 3:
		
		print ''
		print 'Usage:', sys.argv[0], '<variable> <pathname> <--first || --last>'
		print ''
		print 'Description:'
		print '    ' + 'Add a pathname in the variable by editing the environment variables'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	variable = sys.argv[1]
	pathname = sys.argv[2]
	first_or_last = args.arg_exists('--first')
	
	# Command
	env.env_variable_add_directory_in_config_file(variable, pathname, first_or_last)
	
	
	sys.exit(0)
