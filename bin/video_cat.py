#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


import os
import os.path
import tempfile


## @brief Return the command to concatenate video (with avconv)
#  @param[in] videos Video filenames
#  @return the command to concatenate video and the temporary filename to be deleted after run the command
def video_cat_cmd(videos):
	
	file = tempfile.NamedTemporaryFile('w', delete = False)
	
	pwd = os.getcwd()
	
	output = ''
	
	for video in videos:
		
		file.write('file \'' + os.path.join(pwd, video) + '\'' + '\n')
		
		if (video != videos[0]): output += '_'
		output += video
	
	file.close()
	
	cmd = 'avconv -f concat -safe 0 -i "' + file.name + '" -c copy "' + output + '"'
	
	return cmd, file.name


import sys
import os

from make_life_simpler import args

from filename_without_overwrite import filename_without_overwrite


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<video_0 video_1 ... video_n>'
		print ''
		print 'Description:'
		print '    ' + 'Concatenate videos into one.'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	videos = sys.argv[1 : ]
	
	
	# Cat video
	cmd, tmp_filename = video_cat_cmd(videos)
	print cmd
	os.system(cmd)
	os.remove(tmp_filename)
	
	
	sys.exit(0)
