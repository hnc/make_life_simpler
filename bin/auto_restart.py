#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


import os

from make_life_simpler import args
from make_life_simpler.system import *


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help():
		
		print ''
		print 'Usage:', sys.argv[0], '<program> [program args]'
		print ''
		print 'Description:'
		print '    ' + 'Run the program with its arguments'
		print '    ' + 'if the program crashes, re run it automatically'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	program_with_args = sys.argv[1 : ]
	
	
	# Run
	while (1):
		
		system(program_with_args)
	
	
	sys.exit(0)
