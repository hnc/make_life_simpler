#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


## @brief Compute the Audacity pourcentage for "Change Tempo" filter to change the fps
#  @param[in] fps_0 First frame per second
#  @param[in] fps_1 Second frame per second
#  @return the Audacity pourcentage for "Change Tempo" filter to change the fps
def audio_change_fps(fps_0, fps_1):
	
	r = fps_1 - fps_0
	r = r / fps_0
	r = r * 100.0
	
	return r


import sys

from make_life_simpler import args


# Program
if __name__ == "__main__":
	
	# Help
	if args.no_args() or args.help() or args.nb_arg() < 2:
		
		print ''
		print 'Usage:', sys.argv[0], '<fps> <fps>'
		print ''
		print 'Description:'
		print '    ' + 'Display the Audacity pourcentage for \"Change Tempo\" filter to change the fps'
		print ''
		print 'Exemple:'
		print '    ' + '25 fps to 23.976 fps'
		print '    ' + '23.976 - 25 = -1.024'
		print '    ' + '-1.024 / 25 = -0.04096 = -4.096 %'
		print ''
		print 'Warning:'
		print '    ' + 'In some countries, Audacity uses , and not . in float (exemple: -4,096 and -4.096)'
		print ''
		print 'Link:'
		print '    ' + 'http://forum.videohelp.com/threads/309877-Audacity-no-pitch-change'
		print ''
		
		sys.exit(0)
	
	
	# Arguments
	fps_0 = float(sys.argv[1])
	fps_1 = float(sys.argv[2])
	
	
	# Display
	print 'To convert', fps_0, 'fps to', fps_1, 'fps:'
	print '- Audacity pourcentage for \"Change Tempo\" filter to change the fps =', audio_change_fps(fps_0, fps_1), '%'
	
	sys.exit(0)
