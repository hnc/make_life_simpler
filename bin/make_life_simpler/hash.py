#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import hashlib


# http://stackoverflow.com/a/3431835

## @brief Get the hash of a file
#  @param[in] filename   Path of filename
#  @param[in] hash_fct   Hash function
#  @param[in] block_size Block size (65536 by default)
#  @return the hash of a file
def hash_of_file(filename, hash_fct, block_size = 65536):
	
	file = open(filename, 'rb')
	
	buf = file.read(block_size)
	
	while len(buf) > 0:
		
		hash_fct.update(buf)
		buf = file.read(block_size)
	
	file.close()
	
	hash =  hash_fct.hexdigest()
	r = ''
	for c in hash: r += c
	
	return r


## @brief Get the md5 of a file
#  @param[in] filename   Path of filename
#  @param[in] block_size Block size (65536 by default)
#  @return the md5 of a file

def md5_of_file(filename, block_size = 65536):
	
	return hash_of_file(filename, hashlib.md5(), block_size)


## @brief Get the sha256 of a file
#  @param[in] filename   Path of filename
#  @param[in] block_size Block size (65536 by default)
#  @return the sha256 of a file

def sha256_of_file(filename, block_size = 65536):
	
	return hash_of_file(filename, hashlib.sha256(), block_size)
