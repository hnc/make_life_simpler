#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import stat
import shutil
import errno


## @brief Get HOME directory
#  @return HOME directory of the user
def home():
	
	return os.path.expanduser('~')


## @brief Get application directory
#  @param[in] application_name Name of the application
#  @return application directory
def app_data(application_name):
	
	if (os.name == 'posix'):
		
		return home() + '/.' + application_name
		
	elif (os.name == 'nt'):
		
		return os.path.join(os.environ['APPDATA'], application_name)
		
	else:
		
		print 'make_life_simpler.app_data: error: the operating system "' + os.name + '" is not supported'
		raise


# http://stackoverflow.com/questions/1213706/what-user-do-python-scripts-run-as-in-windows/1214935#1214935)
def handle_remove_readonly(func, path, exc):
	excvalue = exc[1]
	if func in (os.rmdir, os.remove) and excvalue.errno == errno.EACCES:
		os.chmod(path, stat.S_IRWXU| stat.S_IRWXG| stat.S_IRWXO) # 0777
		func(path)
	else:
		raise

## @brief Remove a directory (and all files and directories within this directory)
#  @param[in] path Path of the directory to be removed
def rm_directory(path):
	shutil.rmtree(path, ignore_errors=False, onerror=handle_remove_readonly)

## @brief Search file or directory (recursive)
#  @param[in] directory Directory where start the search
#  @param[in] filename  File or directory name to be searched
#  @param[in] excludes  Do not search in this path
#  @return the path of the filename found, None otherwise
def search(directory, filename_searched, excludes = []):
	
	directories = [ directory ]
	
	while (len(directories) != 0):
		
		directory = directories.pop(0)
		
		# os.listdir(directory) fails on "C:/Documents and Settings/*.*)
		try:
			
			filenames = os.listdir(directory)
			filenames.sort()
			
			for filename in filenames:
				
				path = os.path.join(directory, filename)
				
				if (filename == filename_searched):
					
					return path
					
				elif (path not in excludes and os.path.isdir(path) and os.access(path, os.R_OK)):
					
					directories.append(path)
		
		except:
			
			pass
	
	return None
