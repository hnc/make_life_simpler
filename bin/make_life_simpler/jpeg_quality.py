#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os

from system import *


## @brief Reduce jpeg quality
#  @param[in] filename        Image filename
#  @param[in] output_filename Output filename
#  @param[in] quality         Output quality (75 by default)
def jpeg_quality(filename, output_filename, quality = 75):
	
	system([ 'convert', '-quality', str(quality), filename, output_filename ])


## @brief Reduce jpeg quality
#  @param[in] dirname        Directory name where are images
#  @param[in] output_dirname Output directory base name (quality_ by default)
#  @param[in] quality        Output quality (75 by default)
def jpeg_quality_dir(dirname, output_dirname = 'quality_', quality = 75):
	
	output_dirname_with_quality = output_dirname + str(quality)
	
	if (not os.path.exists(output_dirname_with_quality)): os.makedirs(output_dirname_with_quality)
	
	for filename in os.listdir(dirname):
		
		pathname = os.path.join(dirname, filename)
		
		if (os.path.isfile(pathname)):
			
			jpeg_quality(pathname, os.path.join(output_dirname_with_quality, filename), quality)
