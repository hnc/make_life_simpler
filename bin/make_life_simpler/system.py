#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import subprocess

from abort_if_no import *


## @brief Excute a command
#  @param[in] cmd_arg_list Command
#  @return return code of the command
def system(cmd_arg_list):
	
	print 'Execute', cmd_arg_list
	
	error_code = subprocess.call(cmd_arg_list)
	return error_code


## @brief Excute a command and abort if command fails
#  @param[in] cmd_arg_list Command
def system_halt_on_error(cmd_arg_list):

	abort_if_no(system(cmd_arg_list) == 0, 'Command "' + str(cmd_arg_list) + '" fails')
