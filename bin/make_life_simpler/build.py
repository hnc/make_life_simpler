#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import os
import os.path

sys.path.insert(1, os.path.join(sys.path[0], '..'))
from get_exe import *
from cmd_with_root_access import *
from system import *
import env


## @brief Return the make install command to install in install_directory
#  @param[in] install_directory Directory for the installation
#  @return the make install command to install in install_directory
def make_install_cmd(install_directory):
	
	cmd = [make(), 'install']
	
	return cmd_with_root_access_if_needed(cmd, install_directory)


## @brief Build with cmake ..
#  @param[in] project_path       Path of project
#  @param[in] install_directory  Directory for the installation
#  @param[in] c_compiler         Custom C compiler
#  @param[in] cpp_compiler       Custom C++ compiler
#  @param[in] cmake_args_list    CMake arguments
#  @param[in] extra_targets_list Other targets for make
def build_with_cmake(project_path, install_directory, c_compiler = '', cpp_compiler = '', cmake_args_list = [], extra_targets_list = []):
	
	original_working_dir = os.getcwd()
	
	print 'Build', project_path

	build_path = os.path.join(project_path, 'build')
	if (not os.path.exists(build_path)): os.makedirs(build_path)
	os.chdir(build_path)
	
	# POSIX
	if (os.name == 'posix'):
		
		cmd = [cmake(), '..', '-DCMAKE_INSTALL_PREFIX=' + install_directory]
		if (c_compiler != ''): cmd += ['-DCMAKE_C_COMPILER=' + c_compiler]
		if (cpp_compiler != ''): cmd += ['-DCMAKE_CXX_COMPILER=' + cpp_compiler]
		cmd += cmake_args_list
		
		system_halt_on_error(cmd)
		
	# NT
	elif (os.name == 'nt'):
		
		cmd = [cmake(), '..', '-DCMAKE_INSTALL_PREFIX=' + install_directory, '-GMinGW Makefiles']
		if (c_compiler != ''): cmd += ['-DCMAKE_C_COMPILER=' + c_compiler]
		if (cpp_compiler != ''): cmd += ['-DCMAKE_CXX_COMPILER=' + cpp_compiler]
		cmd += cmake_args_list
		
		system(cmd)
		system_halt_on_error(cmd)
		
	# Other
	else:
		
		print 'Error: the operating system "' + os.name + '" is not supported'
		
	system_halt_on_error([make()])

	for extra_target in extra_targets_list:
		system_halt_on_error([make(), extra_target])

	system_halt_on_error(make_install_cmd(install_directory))
	
	print ''
	
	os.chdir(original_working_dir)


## @brief Build with autotools
#  @param[in] project_path        Path of project
#  @param[in] install_directory   Directory for the installation
#  @param[in] configure_args_list Option for ./configure
def build_with_autotools(project_path, install_directory, configure_args_list = []):
	
	original_working_dir = os.getcwd()
	
	print 'Build', project_path

	os.chdir(project_path)
	
	if os.path.exists('get_submodules.sh'): system_halt_on_error(['bash', 'get_submodules.sh'])
	
	if os.path.exists('autogen.sh'): system_halt_on_error(['bash', 'autogen.sh'])
	
	system_halt_on_error(['./configure', '--prefix=' + install_directory] + configure_args_list)
	
	system_halt_on_error([make()])
	
	system_halt_on_error(make_install_cmd(install_directory))
	
	print ''
	
	os.chdir(original_working_dir)


def find_mingw_path_on_windows():
	
	mingw_directory_path = os.environ['PROGRAMFILES'] + '\\mingw-builds'
	
	if (os.path.exists(mingw_directory_path)):
		
		for version_path in os.listdir(mingw_directory_path):
			
			mingw_path = mingw_directory_path + '\\' + version_path + '\\mingw32'
			
			if (os.path.exists(mingw_path)):
				
				return mingw_path
	
	return None


## @brief Update C_INCLUDE_PATH, CPLUS_INCLUDE_PATH, LIBRARY_PATH, LD_LIBRARY_PATH, LD_RUN_PATH, PATH, CMAKE_INCLUDE_PATH, CMAKE_LIBRARY_PATH, CMAKE_PREFIX_PATH variables
#  @param[in] pathname      Pathname to be added in environment variable
#  @param[in] first_or_last True for first, False for last
def update_variables(pathname, first_or_last):
	
	include_path = os.path.join(pathname, 'include')
	lib_path     = os.path.join(pathname, 'lib')
	bin_path     = os.path.join(pathname, 'bin')
	
	env.env_variable_add_directory('C_INCLUDE_PATH',     include_path, first_or_last)
	env.env_variable_add_directory('CPLUS_INCLUDE_PATH', include_path, first_or_last)
	env.env_variable_add_directory('LIBRARY_PATH',       lib_path,     first_or_last)
	env.env_variable_add_directory('LD_LIBRARY_PATH',    lib_path,     first_or_last)
	env.env_variable_add_directory('LD_RUN_PATH',        lib_path,     first_or_last)
	env.env_variable_add_directory('PATH',               bin_path,     first_or_last)
	env.env_variable_add_directory('CMAKE_INCLUDE_PATH', include_path, first_or_last)
	env.env_variable_add_directory('CMAKE_LIBRARY_PATH', lib_path,     first_or_last)
	env.env_variable_add_directory('CMAKE_PREFIX_PATH',  pathname,     first_or_last)
	
	if (os.name == 'nt'):
		mingw_path = find_mingw_path_on_windows()
		if (mingw_path != None):
			env.env_variable_add_directory('PATH', mingw_path + '\\bin', first_or_last)
			env.env_variable_add_directory('PATH', mingw_path + '\\lib', first_or_last)


## @brief Update C_INCLUDE_PATH, CPLUS_INCLUDE_PATH, LIBRARY_PATH, LD_LIBRARY_PATH, LD_RUN_PATH, PATH, CMAKE_INCLUDE_PATH, CMAKE_LIBRARY_PATH, CMAKE_PREFIX_PATH variables
#  @param[in] pathname      Pathname to be added in environment variable
#  @param[in] first_or_last True for first, False for last
def update_variables_in_config_files(pathname, first_or_last):
	
	include_path = os.path.join(pathname, 'include')
	lib_path     = os.path.join(pathname, 'lib')
	bin_path     = os.path.join(pathname, 'bin')
	
	env.env_variable_add_directory_in_config_file('C_INCLUDE_PATH',     include_path, first_or_last)
	env.env_variable_add_directory_in_config_file('CPLUS_INCLUDE_PATH', include_path, first_or_last)
	env.env_variable_add_directory_in_config_file('LIBRARY_PATH',       lib_path,     first_or_last)
	env.env_variable_add_directory_in_config_file('LD_LIBRARY_PATH',    lib_path,     first_or_last)
	env.env_variable_add_directory_in_config_file('LD_RUN_PATH',        lib_path,     first_or_last)
	env.env_variable_add_directory_in_config_file('PATH',               bin_path,     first_or_last)
	env.env_variable_add_directory_in_config_file('CMAKE_INCLUDE_PATH', include_path, first_or_last)
	env.env_variable_add_directory_in_config_file('CMAKE_LIBRARY_PATH', lib_path,     first_or_last)
	env.env_variable_add_directory_in_config_file('CMAKE_PREFIX_PATH',  pathname,     first_or_last)
	
	if (os.name == 'nt'):
		mingw_path = find_mingw_path_on_windows()
		if (mingw_path != None):
			env.env_variable_add_directory_in_config_file('PATH', mingw_path + '\\bin', first_or_last)
			env.env_variable_add_directory_in_config_file('PATH', mingw_path + '\\lib', first_or_last)


## @brief Display how update C_INCLUDE_PATH, CPLUS_INCLUDE_PATH, LIBRARY_PATH, LD_LIBRARY_PATH, LD_RUN_PATH, PATH, CMAKE_INCLUDE_PATH, CMAKE_LIBRARY_PATH, CMAKE_PREFIX_PATH variables
#  @param[in] pathname      Pathname to be added in environment variable
#  @param[in] first_or_last True for first, False for last
def update_variables_display(pathname, first_or_last):
	
	include_path = os.path.join(pathname, 'include')
	lib_path     = os.path.join(pathname, 'lib')
	bin_path     = os.path.join(pathname, 'bin')
	
	env.env_variable_add_directory_display('C_INCLUDE_PATH',     include_path, first_or_last)
	env.env_variable_add_directory_display('CPLUS_INCLUDE_PATH', include_path, first_or_last)
	env.env_variable_add_directory_display('LIBRARY_PATH',       lib_path,     first_or_last)
	env.env_variable_add_directory_display('LD_LIBRARY_PATH',    lib_path,     first_or_last)
	env.env_variable_add_directory_display('LD_RUN_PATH',        lib_path,     first_or_last)
	env.env_variable_add_directory_display('PATH',               bin_path,     first_or_last)
	env.env_variable_add_directory_display('CMAKE_INCLUDE_PATH', include_path, first_or_last)
	env.env_variable_add_directory_display('CMAKE_LIBRARY_PATH', lib_path,     first_or_last)
	env.env_variable_add_directory_display('CMAKE_PREFIX_PATH',  pathname,     first_or_last)


## @brief Display help after update_variables_in_config_files
def update_variables_in_config_files_help():
	
	if (os.name == 'posix'):
		
		print 'You must execute "source ~/.bashrc" for each console launched before relogging'
		
	elif (os.name == 'nt'):
		
		print 'Please restart your console'
		
	else:
		print 'Error: the operating system "' + os.name + '" is not supported'


## @brief Display C_INCLUDE_PATH, CPLUS_INCLUDE_PATH, LIBRARY_PATH, LD_LIBRARY_PATH, LD_RUN_PATH, PATH, CMAKE_INCLUDE_PATH, CMAKE_LIBRARY_PATH, CMAKE_PREFIX_PATH variables
def display_variables():
	
	print 'C_INCLUDE_PATH =',     os.environ['C_INCLUDE_PATH']
	print 'CPLUS_INCLUDE_PATH =', os.environ['CPLUS_INCLUDE_PATH']
	print 'LIBRARY_PATH =',       os.environ['LIBRARY_PATH']
	print 'LD_LIBRARY_PATH =',    os.environ['LD_LIBRARY_PATH']
	print 'LD_RUN_PATH =',        os.environ['LD_RUN_PATH']
	print 'PATH =',               os.environ['PATH']
	print 'CMAKE_INCLUDE_PATH =', os.environ['CMAKE_INCLUDE_PATH']
	print 'CMAKE_LIBRARY_PATH =', os.environ['CMAKE_LIBRARY_PATH']
	print 'CMAKE_PREFIX_PATH =',  os.environ['CMAKE_PREFIX_PATH']
