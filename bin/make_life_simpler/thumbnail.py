#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os

from system import *


## @brief Create thumbnail
#  @param[in] filename        Image filename
#  @param[in] output_filename Output filename
#  @param[in] width           Width of thumbnail (128 by default)
def thumbnail(filename, output_filename, width = 128):
	
	system([ 'convert', '-thumbnail', str(width), filename, output_filename ])


## @brief Create thumbnails
#  @param[in] dirname        Directory name where are images
#  @param[in] output_dirname Output directory (thumbnails by default)
#  @param[in] width          Width of thumbnail (128 by default)
def thumbnails(dirname, output_dirname = 'thumbnails', width = 128):
	
	if (not os.path.exists(output_dirname)): os.makedirs(output_dirname)
	
	for filename in os.listdir(dirname):
		
		pathname = os.path.join(dirname, filename)
		
		if (os.path.isfile(pathname)):
			
			thumbnail(pathname, os.path.join(output_dirname, filename), width)
