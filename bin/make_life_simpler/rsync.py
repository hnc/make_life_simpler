#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import os.path


## @brief Sync directories
#  @param[in] server_ip   Ip of server
#  @param[in] login       Login
#  @param[in] source_root Root directory
#  @param[in] sources     Directory to be downloaded
#  @param[in] destination Destination
#  @param[in] excludes    Not download theses files or directories ([] by default)
#  @param[in] port        Port of server (22 is the common SSH port) ('' by default)
#  @param[in] verbose     Verbose (True by default)
def sync(server_ip, login, source_root, sources, destination, excludes = [], port = '', verbose = True):
	
	for source in sources:
		
		rsync_cmd = 'rsync --protect-args '
		
		if (port == ''): rsync_cmd += '-e ssh '
		else: rsync_cmd += '-e "ssh -p ' + port + '" '
		
		if (verbose): rsync_cmd += '-aP '
		else: rsync_cmd += '-a '
		
		rsync_cmd += '"' + login + '@' + server_ip + ':' + os.path.join(source_root, source) + '*' + '" '
		
		rsync_cmd += '"' + os.path.join(destination, source) + '" '
		
		for exclude in excludes:
			rsync_cmd += '--exclude "' + exclude + '" '
		
		if (verbose): print rsync_cmd
		
		os.system(rsync_cmd)


## @brief Download one file
#  @param[in] server_ip   Ip of server
#  @param[in] login       Login
#  @param[in] source_root Root directory
#  @param[in] sources     File to be downloaded [(directory, file)]
#  @param[in] destination Destination
#  @param[in] port        Port of server (22 is the common SSH port) ('' by default)
def get(server_ip, login, source_root, sources, destination, port = ''):
	
	for source in sources:
		
		rsync_cmd = 'rsync --protect-args '
		
		if (port == ''): rsync_cmd += '-e ssh '
		else: rsync_cmd += '-e "ssh -p ' + port + '" '
		
		if (verbose): rsync_cmd += '-aP '
		else: rsync_cmd += '-a '
		
		rsync_cmd += '"' + login + '@' + server_ip + ':' + os.path.join(source_root, source) + '"  '
		
		rsync_cmd += '"' + os.path.join(destination, source) + '" '
		
		if (verbose): print rsync_cmd
		
		os.system(rsync_cmd)
