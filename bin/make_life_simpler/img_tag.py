#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os

from system import *


## @brief Compose tag image over an image
#  @param[in] img       Image filename
#  @param[in] tag Image (Tag) filename
#  @param[in] output    Output filename
def img_tag(img, tag, output):
	
	system(['composite', '-compose', 'Over', '-gravity', 'SouthEast', tag, img, output])


## @brief Compose tag image over images
#  @param[in] dirname        Directory name where are images
#  @param[in] tag Image (Tag) filename
#  @param[in] output_dirname Output directory base name (tag by default)
def img_tag_dir(dirname, tag, output_dirname = 'tag'):
	
	if (not os.path.exists(output_dirname)): os.makedirs(output_dirname)
	
	for filename in os.listdir(dirname):
		
		pathname = os.path.join(dirname, filename)
		
		if (os.path.isfile(pathname)):
			
			img_tag(pathname, tag, os.path.join(output_dirname, filename))
