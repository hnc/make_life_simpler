#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys
import urllib
import distutils.spawn


## @brief Download a file
#  @param[in] url             First part of the URL
#  @param[in] filename        Second part of the URL
#  @param[in] output_filename Output filename (use filename by default)
def download(url, filename, output_filename = ''):
	
	# Get output_filename
	if (output_filename == ''): output_filename = filename
	
	# Download with urllib
	try:
		image = urllib.URLopener()
		image.retrieve(url + filename, output_filename)
		
	# Download with urllib fails
	except:
		
		# Download with wget
		if (distutils.spawn.find_executable('wget') != None):
			if (os.system('wget ' + url + filename + ' -O ' + output_filename) != 0): os.remove(output_filename)
		
		# Download with curl
		elif (distutils.spawn.find_executable('curl') != None):
			os.system('curl -L "' + url + filename + '" -o "' + output_filename + '"')
			
		elif (os.name == 'nt'):
			os.system('powershell -Command (new-object System.Net.WebClient).DownloadFile(\'' + url + filename + '\',\'' + output_filename + '\')')
		
		# Download fails
		else: raise
	
	# Not found
	if (not os.path.exists(filename)): raise
