#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys


## @brief Return number of arguments (the name of program is ignored)
#  @return number of arguments
def nb_arg():
	
	return len(sys.argv) - 1


## @brief No argument? (the name of program is ignored)
#  @return True if no argument, False otherwise
def no_args():
	
	return len(sys.argv) == 1


## @brief Argument exist?
#  @param argument Argument
#  @return True if the argument exists, False otherwise
def arg_exists(argument):
	
	return argument in sys.argv


## @brief -h or --help or -? is in the arguments?
#  @return True if -h or --help or -? is in the arguments, False otherwise
def help():
	
	if arg_exists("-h"): return True
	if arg_exists("--help"): return True
	if arg_exists("-?"): return True
	
	return False


## @brief Get next argument
#  @param argument Argument
#  @return next argument if it exists, empty string otherwise
def next_arg(argument):
	
	try:
		
		i = sys.argv.index(argument)
		if (len(sys.argv) > i): return sys.argv[i + 1]
		else: return ''
	
	except:
		
		return ''
