#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys


## @brief Abort if the condition is False
#  @param[in] yes_or_no  Condition (True or False)
#  @param[in] error_code Error code (0 by default)
#  @param[in] error_msg  Error message ('Aborting...   _n' by default)
def abort_if_no(yes_or_no, error_code = 0, error_msg = 'Aborting...\n'):
	
	if (yes_or_no == False):
		
		print error_msg
		
		sys.exit(error_code)
