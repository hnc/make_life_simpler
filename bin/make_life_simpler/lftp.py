#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import os.path


## @brief Return the end of the lftp command
#  @return the end of the lftp command
def end_lftp_cmd():
	
	return '; set ftp:ssl-force true ; set ftp:ssl-protect-data true ; quit'


## @brief Sync directories
#  @param[in] server_ip   Ip of server
#  @param[in] port        Port of server (21 is the common FTP port)
#  @param[in] login       Login
#  @param[in] password    Password
#  @param[in] sources     Directory to be downloaded
#  @param[in] destination Destination
#  @param[in] excludes    Not download theses files or directories ([] by default)
#  @param[in] verbose     Verbose (True by default)
def sync(server_ip, port, login, password, sources, destination, excludes = [], verbose = True):
	
	for source in sources:
		
		lftp_cmd = 'mirror -c '
		if (verbose): lftp_cmd += '-vvv '
		lftp_cmd += '\'' + source + '\'' + ' ' + '\'' + os.path.join(destination, source) + '\'' + ' '
		for exclude in excludes: lftp_cmd += '-X ' + '\'' + exclude + '\'' + ' '
		lftp_cmd += end_lftp_cmd()
		
		lftp_cmd = 'lftp ftp://' + login + ':' + password + '@' + server_ip + ':' + port + ' -e "' + lftp_cmd + '"'
		
		os.system(lftp_cmd)


## @brief Download one file
#  @param[in] server_ip   Ip of server
#  @param[in] port        Port of server (21 is the common FTP port)
#  @param[in] login       Login
#  @param[in] password    Password
#  @param[in] sources     File to be downloaded [(directory, file)]
#  @param[in] destination Destination
def get(server_ip, port, login, password, sources, destination):
	
	for source in sources:
		
		destination_without_antislash = os.path.join(destination, source[0]).replace('\\', '')
		if not os.path.exists(destination_without_antislash): os.makedirs(destination_without_antislash)
		
		lftp_cmd = 'get ' + '-O ' + '\'' + os.path.join(destination, source[0]) + '\'' + ' ' + '\'' + source[0] + source[1] + '\'' + ' '
		lftp_cmd += end_lftp_cmd()
		
		lftp_cmd = 'lftp ftp://' + login + ':' + password + '@' + server_ip + ':' + port + ' -e "set xfer:clobber false ; ' + lftp_cmd + '"'
		
		os.system(lftp_cmd)
