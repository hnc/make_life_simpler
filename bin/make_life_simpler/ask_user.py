#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from abort_if_no import *


## @brief Return True if user answers y, false otherwise
#  @param[in] question Question for user
#  @return True if user answers y, false otherwise
def yes_no_question(question):
	
	sys.stdout.write(question + ' (y/n) = ')
	
	sys.stdout.flush()
	
	yes_or_no = raw_input()
	
	return (yes_or_no.lower() == 'y')


## @brief Return choice number
#  @param[in] choices  Choices
#  @param[in] question Question for user
#  @return choice number
def choice_question(choices, question):
	
	abort_if_no(len(choices) != 0)
	
	print 'Choices:'
	for i in range(0, len(choices)):
		print '[' + str(i) + '] ' + choices[i]
	
	i = -1
	while (i < 0 or i >= len(choices)):
		sys.stdout.write(question + ' ' + str(range(len(choices))) + ' = ')
		sys.stdout.flush()
		i = int(raw_input())
	
	return i
