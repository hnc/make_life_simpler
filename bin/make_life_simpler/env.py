#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import sys


## @brief Add pathname in a environment variable
#  @param[in] variable      Environment variable name
#  @param[in] pathname      Pathname to be added in environment variable
#  @param[in] first_or_last True for first, False for last
def env_variable_add_directory(variable, pathname, first_or_last):
	
	if (variable in os.environ):
		
		# POSIX
		if (os.name == 'posix'):
			
			if (variable == 'PATH'):
				
				if (first_or_last): os.environ[variable] = pathname + ':' + os.environ[variable]
				else: os.environ[variable] += ':' + pathname
				
			else:
				
				if (first_or_last): os.environ[variable] = ':' + pathname + os.environ[variable]
				else: os.environ[variable] += ':' + pathname
			
		# NT
		elif (os.name == 'nt'):
			
			if (first_or_last): os.environ[variable] = ';' + pathname + os.environ[variable]
			else: os.environ[variable] += ';' + pathname
			
		# Other
		else:
			
			print 'Error: the operating system "' + os.name + '" is not supported'
	
	else:
		
		# POSIX
		if (os.name == 'posix'):
			
			if (variable == 'PATH'): os.environ[variable] = pathname
			
			else: os.environ[variable] = ':' + pathname
			
		# NT
		elif (os.name == 'nt'):
			
			os.environ[variable] = ';' + pathname
			
		# Other
		else:
			
			print 'Error: the operating system "' + os.name + '" is not supported'


## @brief Display how add pathname in a environment variable
#  @param[in] variable      Environment variable name
#  @param[in] pathname      Pathname to be added in environment variable
#  @param[in] first_or_last True for first, False for last
def env_variable_add_directory_display(variable, pathname, first_or_last):
	
	# POSIX
	if (os.name == 'posix'):
		
		if (variable == 'PATH'):
			
			if (first_or_last): print 'export ' + variable + '="' + pathname + ':$' + variable + '"'
			else: print 'export ' + variable + '="$' + variable + ':' + pathname + '"'
			
		else:
			
			if (first_or_last): print 'export ' + variable + '=":' + pathname + '$' + variable + '"'
			else: print 'export ' + variable + '="$' + variable + ':' + pathname + '"'
		
	# NT
	elif (os.name == 'nt'):
		
		if (first_or_last):  print 'set ' + variable + '=' + pathname + ';%' + variable + '%'
		else: print 'set ' + variable + '=%' + variable + '%;' + pathname
		
	# Other
	else:
		
		print 'Error: the operating system "' + os.name + '" is not supported'


## @brief Add pathname in a environment variable in configuration file
#  @param[in] variable      Environment variable name
#  @param[in] pathname      Pathname to be added in environment variable
#  @param[in] first_or_last True for first, False for last
def env_variable_add_directory_in_config_file(variable, pathname, first_or_last):
	
	# POSIX
	if (os.name == 'posix'):
		
		# Add /opt/local in environment variables if OS X
		if (sys.platform == 'darwin'):
			
			if (not os.path.exists(os.environ['HOME'] + '/.bashrc_sourced_by_make_life_simpler')):
				
				# Modify .profile
				profile = open(os.environ['HOME'] + '/.profile', 'a')
				profile.write('\n')
				profile.write('source ~/.bashrc\n')
				profile.close()
				
				# Create .bashrc_sourced_by_make_life_simpler
				done = open(os.environ['HOME'] + '/.bashrc_sourced_by_make_life_simpler', 'w')
				done.close()
		
		# PATH variable
		if (variable == 'PATH'):
		
			profile = open(os.environ.get('HOME') + '/.profile', 'a')
			
			profile.write('\n')
			profile.write('# Modify environment variable by make_life_simpler https://gitlab.com/hnc/make_life_simpler' + '\n')
			
			if (first_or_last):
				
				profile.write('if [ -d "' + pathname + '" ] ; then' + '\n')
				profile.write('\tPATH=":' + pathname + ':$PATH"' + '\n')
				profile.write('fi' + '\n')
				
			else:
				
				profile.write('if [ -d "' + pathname + '" ] ; then' + '\n')
				profile.write('\tPATH="' + '$PATH:' + pathname + '"' + '\n')
				profile.write('fi' + '\n')
			
			profile.close()
		
		# Other variable
		else:
		
			bashrc_file = open(os.path.join(os.environ['HOME'], '.bashrc'), 'a')
			
			bashrc_file.write('\n')
			bashrc_file.write('# Modify environment variable by make_life_simpler https://gitlab.com/hnc/make_life_simpler' + '\n')
			
			if (first_or_last):
				
				# variable start by : if not
				bashrc_file.write('if [[ "$' + variable + '" != "" ]]; then' + '\n')
				bashrc_file.write('\t' + 'if [[ "$' + variable + '" != :* ]]; then' + '\n')
				bashrc_file.write('\t' + '\t' + 'export ' + variable + '=":$' + variable + '"' + '\n')
				bashrc_file.write('\t' + 'fi' + '\n')
				bashrc_file.write('fi' + '\n')
				
				# Add path is the path is not the first path
				bashrc_file.write('if [[ "$' + variable + '" != :' + pathname + '* ]]; then' + '\n')
				bashrc_file.write('\t' + 'export ' + variable + '=":' + pathname + '$' + variable + '"' + '\n')
				bashrc_file.write('fi' + '\n')
				
			else:
				
				bashrc_file.write('if [[ "$' + variable + '" != *' + pathname + '* ]]; then' + '\n')
				bashrc_file.write('\t' + 'export ' + variable + '="$' + variable + ':' + pathname + '"' + '\n')
				bashrc_file.write('fi' + '\n')
			
			bashrc_file.close()
		
	# NT
	elif (os.name == 'nt'):
		
		#print 'Change the environment variable in Microsoft Windows 7:'
		#print ''
		#print 'In "Control Panel"'
		#print ' > "System"'
		#print ' > "Advanced System Settings" tab'
		#print ' > "Environment variables"'
		#print ''
		#print 'Search the variable "' + variable + '"'
		#print 'and add the path "' + pathname + '"'
		#print '(The paths are separated by ;)'
		
		# http://technet.microsoft.com/en-us/library/cc755104%28v=ws.10%29.aspx
		
		cmd = ''
		
		if (first_or_last): cmd = 'setx ' + variable + ' "' + '%' + variable + '%;' + pathname + '"'
		else: cmd = 'setx ' + variable + ' "' + pathname + ';%' + variable + '%' + '"'
		
		print cmd
		os.system(cmd)
		print ''
		
	# Other
	else:
		
		print 'Error: the operating system "' + os.name + '" is not supported'
