# Archive 2021/11/17

This project is not maintained since 2016.

# Make Life Simpler

Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr <br />
https://gitlab.com/hnc/make_life_simpler

# Make Life Simpler

Collection of (useful) scripts

Apache License, Version 2.0 <br />
GNU Affero General Public License 3+

### System Requirement

Make Life Simpler is written in Python 2.7 <br />
http://www.python.org/

### Installation

Create a symlink of bin directory on your home <br />
or add the bin directory in your PATH environment variable

### Use

See programs in bin directory

### Use

In your Python 2.7 program, add:
```python
sys.path.append('/make_life_simpler/bin/directory') # make_life_simpler bin directory
from make_life_simpler import args
```
