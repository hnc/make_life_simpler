#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2016 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import os


# Copy form https://gitlab.com/hnc/make_life_simpler/
def nb_arg(): return len(sys.argv) - 1
def no_args(): return len(sys.argv) == 1
def arg_exists(argument): return argument in sys.argv
def help():
	if arg_exists('-h'): return True
	if arg_exists('--help'): return True
	if arg_exists('-?'): return True
	return False


# TODO
user = 'USER'
daemon = '/PATH/TO/YOUR/DAEMON/daemon.py'


def usage():
	
	print 'Usage:', sys.argv[0], '< start | stop | restart >'


def start():
	
	cmd = 'su ' + user + ' -c "' + daemon + '"'
	
	print sys.argv[0], 'starts:', daemon
	print cmd
	
	os.system(cmd)


def stop():
	
	cmd = 'pkill -x "' + os.path.basename(daemon) + '"'
	
	print sys.argv[0], 'stops', daemon
	print cmd
	
	os.system(cmd)


# Program
if __name__ == "__main__":
	
	if (help() or no_args()):
		
		usage()
		
		sys.exit(0)
	
	
	if (arg_exists('start')):
		
		start()
	
	elif (arg_exists('stop')):
		
		stop()
	
	elif (arg_exists('restart')):
		
		stop()
		start()
	
	else:
		
		usage()
	
	
	sys.exit(0)
