 -----------------
| Run your daemon |
 -----------------

https://gitlab.com/hnc/make_life_simpler

run_your_deamon is a empty project to run your own daemon


 --------
| How to |
 --------

daemon.py
---------

It is your program, modify it as you want.


daemond.py
----------

It is the script to run daemon.py.
Modify lines 35 and 36 to specify user and the path of your program


Installation on Debian GNU/Linux
--------------------------------

https://wiki.debian.org/Daemon

Copy daemond.py in /etc/init.d/
# cp daemond.py /etc/init.d/

Create a symbolic link in /etc/rc*.d/
# ln -s /etc/init.d/daemond.py /etc/rc2.d/S99daemond.py

Run your service
# /etc/init.d/monitoringd.py start
