# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

https://gitlab.com/hnc/make_life_simpler

 -------------------
| Make Life Simpler |
 -------------------

Collection of (useful) scripts

Apache License, Version 2.0
GNU Affero General Public License 3+


 --------------------
| System Requirement |
 --------------------

Make Life Simpler is written in Python 2.7
http://www.python.org/


 --------------
| Installation |
 --------------

Create a symlink of bin directory on your home
or add the bin directory in your PATH environment variable


 -----
| Use |
 -----

See programs in bin directory


 ----------------
| Use As Library |
 ----------------

In your Python 2.7 program, add:
sys.path.append('/make_life_simpler/bin/directory') # make_life_simpler bin directory
from make_life_simpler import args
