#!/bin/bash


# Copyright © 2013-2015 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


if [ "$1" != "--no_new_install_script" ]
then
	current_path=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
	$current_path/new_install
fi


# Packages
packages=""

# Developement
packages="$packages build-essential gcc-multilib llvm clang gdb valgrind kcachegrind"
packages="$packages git mercurial subversion qgit gitk"
packages="$packages cmake autoconf autotools-dev automake libtool cmake-qt-gui cmake-curses-gui"
packages="$packages doxygen graphviz"
packages="$packages cloc sloccount"
packages="$packages pkg-config"

# C
packages="$packages libglib2.0-dev"
packages="$packages gperf"
packages="$packages flex bison"
packages="$packages libgmp-dev"
packages="$packages libssl-dev"
packages="$packages libglib2.0-dev"
packages="$packages libopenal-dev libsndfile-dev"
packages="$packages libudev-dev"
packages="$packages libusb-dev libusb-1.0-0-dev libusb++-dev"
packages="$packages libsdl-dev libsdl1.2-dev libsdl2-dev"
packages="$packages libtinyxml-dev"
packages="$packages libjpeg-dev"

# C++
packages="$packages libboost-all-dev libpoco-dev libtbb-dev libcppunit-dev"
packages="$packages libois-dev"
packages="$packages google-perftools libgoogle-perftools-dev"
#packages="$packages libopenscenegraph-dev openscenegraph-examples openscenegraph"

# Free desktop
packages="$packages libfreeimage-dev libfreetype6-dev fontconfig"

# OpenGL
packages="$packages libgl1-mesa-dev libglew-dev freeglut3-dev"

# X11
packages="$packages libx11-dev libxi-dev libxext-dev"
packages="$packages libxrender-dev libxt-dev libxaw7-dev libxrandr-dev"
packages="$packages xcb libx11-xcb-dev libxcb-render0-dev libxcb-render-util0-dev libxcb-shape0-dev libxcb-randr0-dev libxcb-xfixes0-dev libxcb-sync0-dev libxcb-shm0-dev libxcb-icccm4-dev libxcb-keysyms1-dev libxcb-image0-dev"
packages="$packages libsm-dev libice-dev"

# GTK
packages="$packages libgtk2.0-dev libgtk-3-dev"

# Compression
packages="$packages zlib1g-dev libzzip-dev"

# BLAS & LAPACK
packages="$packages libopenblas-dev liblapack-dev"

# OpenCL
packages="$packages opencl-dev"

# Web
packages="$packages apache2 php5"

# SQL
packages="$packages postgresql sqlite"

# Scilab
packages="$packages scilab"

# IDE
packages="$packages kdevelop kdevelop-dev"
packages="$packages codeblocks"
packages="$packages qtcreator"


echo "aptitude install $packages"
aptitude install $packages
