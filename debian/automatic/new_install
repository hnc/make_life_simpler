#!/bin/bash


# Copyright © 2013-2015 Lénaïc Bagnères, hnc@singularity.fr

# This file is part of "Make life simpler".

# "Make life simpler" is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# "Make life simpler" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with "Make life simpler". If not, see <http://www.gnu.org/licenses/>


# Debian Codename

debian_codename=`lsb_release -c -s`


# Check if stable is in /etc/apt/sources.list

deb_stable_lines=`cat /etc/apt/sources.list | grep "debian.org/debian/ stable main" | wc -l`
deb_stable_comments=`cat /etc/apt/sources.list | grep "debian.org/debian/ stable main" | grep \# | wc -l`
deb_stable=`expr $deb_stable_lines - $deb_stable_comments`

if [ "$deb_stable" -ge "1" ]
then
	echo "debian.org/debian/ stable main is already in your /etc/apt/sources.list"
else
	echo "" >> /etc/apt/sources.list
	echo "deb http://ftp.fr.debian.org/debian/ stable main" >> /etc/apt/sources.list
fi


# Check if deb-multimedia.org is in /etc/apt/sources.list

deb_multimedia_lines=`cat /etc/apt/sources.list | grep deb-multimedia | wc -l`
deb_multimedia_comments=`cat /etc/apt/sources.list | grep deb-multimedia | grep \# | wc -l`
deb_multimedia=`expr $deb_multimedia_lines - $deb_multimedia_comments`

if [ "$deb_multimedia" -ge "1" ]
then
	echo "www.deb-multimedia.org is already in your /etc/apt/sources.list"
else
	echo "" >> /etc/apt/sources.list
	echo "deb http://www.deb-multimedia.org stable main non-free" >> /etc/apt/sources.list
	echo "deb ftp://ftp.deb-multimedia.org $debian_codename main non-free" >> /etc/apt/sources.list
fi


# Update & upgrade
aptitude update
aptitude install
aptitude full-upgrade
aptitude install
